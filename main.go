package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"os"
)

func sha256one(name string) {
	var file io.Reader

	if name == "-" {
		file = os.Stdin
	} else {
		var err error
		file, err = os.Open(name)
		if err != nil {
			log.Print(err)
			return
		}
	}

	h := sha256.New()

	buffer := make([]byte, 8192)
	for {
		n, err := file.Read(buffer)
		if n > 0 {
			h.Write(buffer[:n])
		}
		if err != nil {
			if err != io.EOF {
				log.Print(err)
			}
			break
		}
	}
	as_hex := hex.EncodeToString(h.Sum(nil))

	fmt.Printf("%s  %s\n", as_hex, name)
}

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		args = []string{"-"}
	}
	for _, arg := range args {
		sha256one(arg)
	}
}
